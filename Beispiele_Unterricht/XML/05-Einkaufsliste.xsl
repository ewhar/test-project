<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl = "http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/einkaufsliste">

        <html>
            <head>
                <style>
                ul {
                    font-size: 17px;
                }
                </style>
            </head>
            <body>
                <h1>Unsere Einkaufsliste</h1>
                <p>Bitte kauf uns doch ein:</p>
                <ul>
                    <xsl:for-each select="eintrag">
                        <xsl:sort select="name" order="ascending"></xsl:sort>
                        <li>
                            <xsl:value-of select="name"/>
                            <xsl:value-of select="menge"/>
                        </li>
                    </xsl:for-each>
                </ul>

                <xsl:choose>
                    <xsl:when test="count(//eintrag) =1">
                        <p>Die Einkaufsliste hast <xsl:value-of select="count(//eintrag)"/>
 Eintrag </p>
                    </xsl:when>
                    <xsl:otherwise>
                        <p>Die Einkaufsliste hast <xsl:value-of select="count(//eintrag)"/>
 Einträge</p>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
        </body>
    </html>

</xsl:template>

</xsl:stylesheet>