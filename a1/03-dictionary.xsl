<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
   xmlns:xsl = "http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/dictionary">
      <html>
         <head>
            <title>Wörterbuch</title>
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"/>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
         </head>
         <body class="test">
            <div class="container">
               <h1 class="jumbotron text-center">Wörterliste</h1>
               <div class="table-responsive">
                  <table class="table table-hover text-center">
                     <thead>
                        <tr>
                           <th class="text-center h5">Englisch</th>
                           <th class="text-center h5">Deutsch</th>
                           <th class="text-center h5">Kategorien</th>
                        </tr>
                     </thead>
                     <tbody>
                        <xsl:for-each select="word">
                           <xsl:sort select="@value" order="descending"></xsl:sort>
                           <tr>
                              <td>
                                 <xsl:value-of select="@value"/>
                              </td>
                              <td>
                                 <xsl:if test="translation/ @lang = 'DE'">
                                    <xsl:value-of select="translation"/>
                                 </xsl:if>
                              </td>
                              <td>
                                 <xsl:choose>
                                    <xsl:when test="category = 'Geography'">
                                       <p>
                                          (&#127757;) 
                                          <xsl:value-of select="category"/>
                                       </p>
                                    </xsl:when>
                                    <xsl:when test="category = 'Animal'">
                                       <p>
                                          (&#128568;) 
                                          <xsl:value-of select="category"/>
                                       </p>
                                    </xsl:when>
                                    <xsl:when test="category = 'Food'">
                                       <p>
                                          (&#129385;) 
                                          <xsl:value-of select="category"/>
                                       </p>
                                    </xsl:when>
                                    <xsl:otherwise>
                                       <p>
                                          (&#128213;) 
                                          <xsl:value-of select="category"/>
                                       </p>
                                    </xsl:otherwise>
                                 </xsl:choose>
                              </td>
                           </tr>
                        </xsl:for-each>
                     </tbody>
                  </table>
               </div>
               <!-- STATISTIK-->
               <h1 class="display-3 text-center">Statistik</h1>
               <p class="h5 text-center">
                  Die Liste enthält insgesamt 
                  <xsl:choose>
                     <xsl:when test="count(//word) =1">
                        <strong>
                           <u>
                              <xsl:value-of select="count(//word)"/>
                              Vokabel
                           </u>
                        </strong>
                     </xsl:when>
                     <xsl:otherwise>
                        <strong>
                           <u>
                              <xsl:value-of select="count(//word)"/>
                              Vokabeln
                           </u>
                        </strong>
                     </xsl:otherwise>
                  </xsl:choose>
               </p>
               <ul class="list-group list-group-flush text-center">
                  <li class="list-group-item text-center">
                     <u>Deutsch: </u>
                     <xsl:value-of select="count(//translation[@lang='DE'])"/>
                  </li>
                  <li class="list-group-item text-center">
                     <u>Französisch: </u>
                     <xsl:value-of select="count(//translation[@lang='FR'])"/>
                  </li>
                  <li class="list-group-item text-center">
                     <u>Latein: </u>
                     <xsl:value-of select="count(//translation[@lang='LA'])"/>
                  </li>
               </ul>
            </div>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
