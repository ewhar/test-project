let myHeaders = new Headers();
myHeaders.append("Authorization", "Bearer fe5bd7e564df4cc3f60a27ae54361a5e995860c8a28f77ca97735a6047395c0a");

let myHeadersPost = new Headers();
myHeadersPost.append("Authorization", "Bearer fe5bd7e564df4cc3f60a27ae54361a5e995860c8a28f77ca97735a6047395c0a");
myHeadersPost.append("Content-Type", "application/json");

function init(){
    document.getElementById("name").value = '';
    document.getElementById("email").value = '';
    document.getElementById("gender").value = '';
    document.getElementById("status").checked = false;

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch("https://gorest.co.in/public/v2/users", requestOptions)
        .then(response => response.text())
        .then(result => {
            let first = true;
            let json = JSON.parse(result);
            json.forEach((element) => {
                if (first) {
                    first = false;
                    document.getElementById("name").value = element.name;
                    document.getElementById("email").value = element.email;
                    document.getElementById("gender").value = element.gender;
                    if (element.status === 'active')
                        document.getElementById("status").checked = true;
                    else
                        document.getElementById("status").checked = false;
                }
                let list = document.createElement("option");
                list.innerHTML = element.name;
                list.setAttribute("id", element.id);
                document.getElementById("dropDown").appendChild(list);
            });
        })
        .catch(error => console.log('error', error));

    document.getElementById('dropDown').addEventListener('change', function(e) {
        let idFromUser = e.target.options[e.target.selectedIndex].getAttribute('id');
        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://gorest.co.in/public/v2/users/"+idFromUser, requestOptions)
            .then(response => response.text())
            .then(result => {
                var json = JSON.parse(result);
                document.getElementById("name").value = json.name;
                document.getElementById("email").value = json.email;
                document.getElementById("gender").value = json.gender;
                if (json.status === 'active')
                    document.getElementById("status").checked = true;
                else
                    document.getElementById("status").checked = false;
            })
            .catch(error => console.log('error', error));
    });

    document.getElementById('newUser').addEventListener('click', function (){
        newUser();
    });

    document.getElementById('submitUser').addEventListener('click', function (e){
        editUser(e);
    });
}

function newUser(){
    let name = document.getElementById("name").value;
    let email = document.getElementById("email").value;
    let gender = document.getElementById("gender").value;
    let status = 'inactive';
    if(document.getElementById("status").checked)
        status = 'active';
    if(gender === 'male' || gender === 'female')
    {
        let user = {
            "name": name,
            "email": email,
            "gender": gender,
            "status": status
        };
        user = JSON.stringify(user)
        console.log(user);
        let requestOptions = {
            method: 'POST',
            headers: myHeadersPost,
            body: user,
            redirect: 'follow'
        };
        debugger;

        console.log(requestOptions)
        fetch("https://gorest.co.in/public/v2/users", requestOptions)
            .then(response => response.text())
            .then(result => {
                let json = JSON.parse(result);
                let list = document.createElement("option");
                list.innerHTML = json.name;
                list.setAttribute("id", json.id);
                document.getElementById("dropDown").appendChild(list);
            })
            .catch(error => console.log('error', error));
        document.getElementById("name").value = '';
        document.getElementById("email").value = '';
        document.getElementById("gender").value = '';
        document.getElementById("status").checked = false;
        alert("User created")
    }
    else
        alert("Gender must be male or female");
}

function editUser(e){
    let element = document.getElementById("dropDown")
    let editedUser= element.options[element.selectedIndex];
    let idFromUser = editedUser.id;
    console.log("ID " + idFromUser)
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer fe5bd7e564df4cc3f60a27ae54361a5e995860c8a28f77ca97735a6047395c0a");
    myHeaders.append("Content-Type", "application/json");
    let name = document.getElementById("name").value;
    let email = document.getElementById("email").value;
    let gender = document.getElementById("gender").value;
    let status = 'inactive';
    if(document.getElementById("status").checked)
        status = 'active';
    if(gender === 'male' || gender === 'female')
    {
        let user = {
            "name": name,
            "email": email,
            "gender": gender,
            "status": status
        };
        user = JSON.stringify(user)
        console.log(user);
        let requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: user,
            redirect: 'follow'
        };
        fetch("https://gorest.co.in/public/v2/users/"+idFromUser, requestOptions)
            .then(response => response.text())
            .then(result => {
                let json = JSON.parse(result);
                editedUser.setAttribute("id", json.id)
                editedUser.innerHTML = json.name;
            })
            .catch(error => console.log('error', error));
        alert("User updated")

    }
}
