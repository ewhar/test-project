function init(){
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    };
    fetch("https://öä.eu:8090/todo/", requestOptions)
        .then(response => response.text())
        .then(result => {
            let json = JSON.parse(result);
            json.forEach(todo =>{
                let list = document.createElement("li");
                list.setAttribute("id", todo.id);
                list.setAttribute("aufgabe", todo.name);
                list.setAttribute("beschreibung", todo.note);
                list.setAttribute("date", todo.date);
                list.innerHTML = todo.name + " - " + todo.date;
                document.getElementById("ullist").appendChild(list);
            })
        })
        .catch(error => console.log('error', error));
}

function onAddToList() {
    let aufgabe = document.getElementById("aufgabe").value;
    let beschreibung = document.getElementById("textarea").value;
    let datepicker = document.getElementById("datepicker").value;
    if (aufgabe !== "") {
        let todo = {"name": aufgabe,
                "note": beschreibung,
                "date": datepicker
        };
        fetch("https://öä.eu:8090/todo/", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(todo)
        })
            .then(response => response.json())
            .then(data => {
                let todo = JSON.parse(JSON.stringify(data));
                console.log(todo);
                let list = document.createElement("li");
                list.setAttribute("id", todo.id);
                list.setAttribute("aufgabe", todo.name);
                list.setAttribute("beschreibung", todo.note);
                let date = todo.date.slice(0, 10);
                list.setAttribute("date",date);
                list.innerHTML = todo.name + " - " + date;
                document.getElementById("ullist").appendChild(list);
            })
            .catch((error) => {
                console.log(error);
            })
        document.getElementById("aufgabe").value = "";
        document.getElementById("textarea").value = "";
        document.getElementById("datepicker").value = "2022-04-30";
    }else{
        alert("Aufgabe muss einen Titel haben!");
    }
}

document
    .getElementById("ullist")
    .addEventListener("click", (event) => {
        document.getElementById("info").innerHTML = "";
        var tar = getTarget(event);
        let aufgabe = tar.getAttribute("aufgabe");
        let beschreibung = tar.getAttribute("beschreibung");
        let datepicker = tar.getAttribute("date");
        document.getElementById("info").innerHTML =  aufgabe + " " + beschreibung + " " + datepicker;
    });

document
    .getElementById("ullist")
    .addEventListener("dblclick", (event) => {
        document.getElementById("info").innerHTML = "";
        var tar = getTarget(event);
        let id = tar.getAttribute("id");
        var requestOptions = {
            method: 'DELETE',
            redirect: 'follow'
        };
        fetch("https://öä.eu:8090/todo/"+id, requestOptions)
            .then(response => response.text())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
        tar.remove();
    });

// Quelle: https://stackoverflow.com/questions/5116929/get-clicked-li-from-ul-onclick
function getTarget(e) {
    e = e || window.event;
    return e.target || e.srcElement;
}