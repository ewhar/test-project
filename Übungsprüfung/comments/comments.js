function apiGET(){
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    };

    fetch("https://gorest.co.in/public/v2/comments", requestOptions)
        .then(response => response.text())
        .then(result => {
            let json = JSON.parse(result);
            json.forEach(comment =>{
                let list = document.createElement("li");
                list.setAttribute("id", comment.id);
                list.setAttribute("post_id", comment.post_id);
                list.setAttribute("name", comment.name);
                list.setAttribute("email", comment.email);
                list.setAttribute("body", comment.body);
                list.innerHTML = comment.name + " - " + comment.email;
                document.getElementById("ullist").appendChild(list);
            })
        })
        .catch(error => console.log('error', error));
}

function addComment() {
    let name = document.getElementById("name").value;
    let post_id = document.getElementById("post_id").value;
    let email = document.getElementById("email").value;
    let body = document.getElementById("body").value;

    if (name !== "" || email !== "" || post_id !== "") {
        let comment = {
            "post_id": post_id,
            "name": name,
            "email": email,
            "body": body
        };
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer fe5bd7e564df4cc3f60a27ae54361a5e995860c8a28f77ca97735a6047395c0a");
        myHeaders.append("Content-Type", "application/json");
        comment = JSON.stringify(comment);
        console.log(comment);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: comment,
            redirect: 'follow'
        };
        fetch("https://gorest.co.in/public/v2/comments", requestOptions)
            .then(response => response.json())
            .then(data => {
                let list = document.createElement("li");
                list.setAttribute("id", data.id);
                list.setAttribute("post_id", data.post_id);
                list.setAttribute("name", data.name);
                list.setAttribute("email", data.email);
                list.setAttribute("body",data.body);
                list.innerHTML = data.name + " - " + data.email;
                document.getElementById("ullist").appendChild(list);
            })
            .catch((error) => {
                console.log(error);
            })
        document.getElementById("post_id").value = "";
        document.getElementById("name").value = "";
        document.getElementById("email").value = "";
        document.getElementById("body").value = "";
    }else{
        alert("Comment muss einen Namen und eine Email haben!");
    }
}


document
    .getElementById("ullist")
    .addEventListener("click", (event) => {
        document.getElementById("info").innerHTML = "";
        let tar = getTarget(event);
        let name = tar.getAttribute("name");
        let email = tar.getAttribute("email");
        let beschreibung = tar.getAttribute("body");
        document.getElementById("info").innerHTML =  name + "- " + email + ": " + beschreibung;
    });

document
    .getElementById("ullist")
    .addEventListener("dblclick", (event) => {
        document.getElementById("info").innerHTML = "";
        var tar = getTarget(event);
        let id = tar.getAttribute("id");
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer fe5bd7e564df4cc3f60a27ae54361a5e995860c8a28f77ca97735a6047395c0a");
        var requestOptions = {
            method: 'DELETE',
            headers: myHeaders,
            redirect: 'follow'
        };
        fetch("https://gorest.co.in/public/v2/comments/"+id, requestOptions)
            .then(response => response.text())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
        tar.remove();
    });

// Quelle: https://stackoverflow.com/questions/5116929/get-clicked-li-from-ul-onclick
function getTarget(e) {
    e = e || window.event;
    return e.target || e.srcElement;
}

