function init() {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    };
    fetch("https://öä.eu:8090/news/ewaldhartmann/", requestOptions)
        .then(response => response.text())
        .then(result => {
            var json = JSON.parse(result);
            json.forEach(post =>{
                let div = document.createElement("div");
                div.setAttribute("id", post.id);
                let h1 = document.createElement("h1");
                h1.setAttribute("title", post.title);
                h1.innerHTML = post.title;
                div.appendChild(h1);
                if(post.category !== ""){
                    let cat = document.createElement("p");
                    cat.setAttribute("cat", post.category);
                    cat.setAttribute("style", "color:lightgray");

                    cat.innerHTML = "Kategorie: " + post.category;
                    div.append(cat);
                }
                let p1 = document.createElement("p");
                p1.setAttribute("description", post.description);
                p1.innerHTML = post.description;
                div.appendChild(p1)
                let p2 = document.createElement("p");
                p2.setAttribute("redakteur", post.redakteur);
                p2.innerHTML = post.title +" (" + post.date + ")";
                div.appendChild(p2);
                document.getElementById("showlist").appendChild(div);
            })
        })
        .catch(error => console.log('error', error));
}

function addComment(){
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    let title = document.getElementById("title").value;
    let description = document.getElementById("comment").value;
    let category = document.getElementById("cat").value;
    let date = document.getElementById("datepicker").value;
    if (title !== "" && description !== "" && date !== ""){
        let body = JSON.stringify({
            "title": title,
            "description": description,
            "category":category,
            "date": date
        });

        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: body,
            redirect: 'follow'
        };
        fetch("https://öä.eu:8090/news/ewaldhartmann", requestOptions)
            .then(response => response.text())
            .then(result => {
                let post = JSON.parse(result);
                let div = document.createElement("div");
                div.setAttribute("id", post.id);
                let h1 = document.createElement("h1");
                h1.setAttribute("title", post.title);
                h1.innerHTML = post.title;
                div.appendChild(h1);
                if(post.category !== ""){
                    let cat = document.createElement("p");
                    cat.setAttribute("cat", post.category);
                    cat.setAttribute("style", "color:lightgray");

                    cat.innerHTML = "Kategorie: " + post.category;
                    div.append(cat);
                }
                let p1 = document.createElement("p");
                p1.setAttribute("description", post.description);
                p1.innerHTML = post.description;
                div.appendChild(p1)
                let p2 = document.createElement("p");
                p2.setAttribute("redakteur", post.redakteur);
                p2.innerHTML = post.title +" (" + post.date + ")";
                div.appendChild(p2);
                document.getElementById("showlist").appendChild(div);
            })
            .catch(error => console.log('error', error));
    }
    else {
        let title = document.getElementById("title");
        title.setAttribute("style", "background-color:red;")
        let description = document.getElementById("comment");
        description.setAttribute("style", "background-color:red;")
        let date = document.getElementById("datepicker");
        date.setAttribute("style", "background-color:red;")
    }
}

document
    .getElementById("showlist")
    .addEventListener("dblclick", (event) => {
        var tar = getTarget(event);
        var parent = tar.parentNode;
        let id = tar.parentNode.getAttribute("id");
        console.log(id)
        var requestOptions = {
            method: 'DELETE',
            redirect: 'follow'
        };

        fetch("https://öä.eu:8090/news/ewaldhartmann/"+id, requestOptions)
            .then(response => response.text())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
        parent.remove();
    });

// Quelle: https://stackoverflow.com/questions/5116929/get-clicked-li-from-ul-onclick
function getTarget(e) {
    e = e || window.event;
    return e.target || e.srcElement;
}